# Arch Linux Install

#### Goals
1. Full system LUKS2 encryption with `/boot` included.
2. Secure boot sbctl.
3. BTRFS filesystem for rollbacks.

#### Justification
Because we can, not that we need to.

#### Boot into Arch Linux ISO

#### 1. UEFI Check
If this directory does not exist or is empty then you are not booted in UEFI system and <b><i> DO NOT PROCEED WITH THIS INSTALLATION GUIDE. </i></b>
```
ls /sys/firmware/efi/efivars
```

#### 2. Keyboard
```
loadkeys us
```

#### 3. Fonts
```
# Available fonts
ls /usr/share/kbd/consolefonts

setfont ter-120n
```

#### 4. Internet
##### 4.1 Wifi
```
# Check your device name
iwctl station list

iwctl station YOUR-DEVICE-NAME scan
iwctl station YOUR-DEVICE-NAME connect WIFI-NAME
```
##### 4.2 Ethernet
It should work out of the box.

#### 5. Timezone
```
# Check your zone
ls /usr/share/zoneinfo

# Check your location
ls /usr/share/zoneinfo/YOUR-ZONE

timedatectl set-timezone YOUR-ZONE/YOUR-LOCATION
```

#### 6. Disk Partition

##### 6.1 Layout

###### 6.1.1 Dual boot
My example layout. Parition 1 to 5 are made by Windows installation. I created 6th parition for Arch Linux. If yours is like this then make sure you do not format EFI parition. In this case `nvme0n1p1`.
```
/dev/nvme0n1 >
    /dev/nvme0n1p1 - EFI System
    /dev/nvme0n1p2 - Windows Reserved
    /dev/nvme0n1p3 - Windows installation
    /dev/nvme0n1p4 - Windows recovery partition
    /dev/nvme0n1p5 - General NTFS storage parition
    /dev/nvme0n1p6 - Arch Linux Partition
```
###### 6.1.2 Single boot
If you are installing on whole drive then it could look like this after partition.
```
/dev/disk >
    /dev/disk_p1 - EFI System
    /dev/disk_p2 - Arch Linux Parition
```
###### 6.1.3 Misc
1. We will use swapfile so separate swap partition is <b><i>not needed</i></b>.
2. We will create home subvolume in btrfs so we <b><i>do not need</i></b> any other partitions like home here.

##### 6.2 Partioning
Use your favourite utility to parition disk if needed. What we need is one EFI parition and one Linux partition. From now on `/dev/disk_p1` is EFI partition and `/dev/disk_p2` is Linux parition.

###### 6.2.2 Format EFI Parition
> <b>This will erase all data on /dev/disk_p1 !! </b>

> Only do this if you created EFI parition and no other bootloader (eg. Windows) is installed.
```
mfks.fat -F 32 -n EFI_PART /dev/disk_p1
```

###### 6.2.1 Wipe Linux Partition
> <b>This will erase all data on /dev/disk_p2 !! </b>
```
cryptsetup open --type plain -d /dev/urandom /dev/disk_p2 to_be_wiped
dd bs=1M if=/dev/zero of=/dev/mapper/to_be_wiped status=progress
cryptsetup close to_be_wiped
```

#### 7. Encryption
This command will ask for confirmation and password. This password will be used for decryption. <b><i>Remember it!</i></b>
```
cryptsetup --pbkdf pbkdf2 -s 512 -h sha512 -i 3000 luksFormat /dev/disk_p2
```

#### 8. BTRFS

##### 8.1 Unlock Linux Partition
```
cryptsetup open /dev/disk_p2 cryptroot
```

##### 8.2 Format BTRFS
```
mkfs.btrfs -L btrfsroot -n 32k /dev/mapper/cryptroot
```

##### 8.3 Mount BTRFS
```
mount /dev/mapper/cryptroot /mnt
```

##### 8.4 Subvolume Layout
This layout is from arch wiki and suse docs.

| Subvolume                     | Mount point                  |
| ----------------------------- | ---------------------------- |
| <tt>\@                        | <tt>/                        |
| <tt>\@crypt                   | <tt>/crypt                   |
| <tt>\@home                    | <tt>/home                    |
| <tt>\@opt                     | <tt>/opt                     |
| <tt>\@root                    | <tt>/root                    |
| <tt>\@srv                     | <tt>/srv                     |
| <tt>\@swap                    | <tt>/swap                    |
| <tt>\@tmp                     | <tt>/tmp                     |
| <tt>\@snapshots               | <tt>/.snapshots              |
| <tt>\@usr/local               | <tt>/usr/local               |
| <tt>\@var/cache               | <tt>/var/cache               |
| <tt>\@var/crash               | <tt>/var/crash               |
| <tt>\@var/lib/docker          | <tt>/var/lib/docker          |
| <tt>\@var/lib/libvirt/images  | <tt>/var/lib/libvirt/images  |
| <tt>\@var/lib/machines        | <tt>/var/lib/machines        |
| <tt>\@var/lib/mailman         | <tt>/var/lib/mailman         |
| <tt>\@var/lib/mariadb         | <tt>/var/lib/mariadb         |
| <tt>\@var/lib/mysql           | <tt>/var/lib/mysql           |
| <tt>\@var/lib/named           | <tt>/var/lib/named           |
| <tt>\@var/lib/pacman          | <tt>/var/lib/pacman          |
| <tt>\@var/lib/postgresql      | <tt>/var/lib/postgresql      |
| <tt>\@var/log                 | <tt>/var/log                 |
| <tt>\@var/spool               | <tt>/var/spool               |
| <tt>\@var/tmp                 | <tt>/var/tmp                 |



##### 8.5 Create subvolumes
```
cd /mnt
btrfs subvolume create @
mkdir @/0
mkdir @usr
btrfs subvolume create @/0/snapshot
for i in {home,root,srv,opt,var,tmp,swap,crypt,usr/local}; do btrfs subvolume create @${i}; done
btrfs subvolume set-default @/0/snapshot
cd
umount /mnt
```

##### 8.6 Mount Subvolumes
```
mount -o compress-force=zstd,subvol=@/0/snapshot /dev/mapper/cryptroot /mnt
cd /mnt
for i in {home,root,srv,opt,var,tmp,swap,crypt,usr,usr/local,.snapshots}; do mkdir ${i}; done
mount -o compress-force=zstd,subvol=@ /dev/mapper/cryptroot .snapshots
for i in {home,root,srv,opt,var,tmp,swap,crypt,usr/local}; do mount -o compress-force=zstd,subvol=@${i} /dev/mapper/cryptroot ${i}; done
cd
```

#### 9. Mount EFI
```
mkdir -p /mnt/efi
mount /dev/disk_p1 /mnt/efi
```

#### 10. Mount Swap file

##### 10.1 Create swap file
Modify size to your liking. Recommended is RAM+RAM/2.
```
btrfs filesystem mkswapfile --size 4g --uuid clear /mnt/swap/swapfile
chattr +C /mnt/swap/swapfile
```

##### 10.2 Mount swap file
```
swapon /mnt/swap/swapfile
```

#### 11. Installation

##### 11.1 Get faster mirrors
```
reflector --sort rate --latest 25 --protocol https --save /etc/pacman.d/mirrorlist --verbose
```

##### 11.2 Install Packages
Necessary packages are shown below. Add more to your liking.
```
pacstrap -K /mnt base base-devel linux linux-firmware amd\intel-ucode btrfs-progs ntfs-3g snapper neovim networkmanager reflector sbctl grub efibootmgr os-prober p7zip cryptsetup
```
##### 11.3 Generate fstab
```
genfstab -U /mnt >> /mnt/etc/fstab
```

###### 11.3.1 Edit fstab
- In fstab, look for root mount point with @/0/snapshot and remove `subvolid=xxx` option.
- Also, check that other mount points are correct and whether swapfile location is okay.

###### 11.3.2 Edit pacman.conf
Arch Linux's package manager stores database in `/var/lib/pacman`. Since we created `/var as subvolume`, it wont be backed up in snapshot. So we will move database location `from /var/lib/pacman to /usr/lib/pacman`

```
/mnt/etc/pacman.conf
[options]
DBPath = /usr/lib/pacman/
```


#### 12. Post-Installation

##### 12.1 Chroot into system
```
arch-chroot /mnt
```

##### 12.2 Timezone
```
ln -sf /usr/share/zoneinfo/YOUR-ZONE/YOUR-LOCATION /etc/localtime
hwclock --systohc
```

##### 12.3 Locale

###### 12.3.1 Edit locale.gen
Uncomment your locale (eg. en_US-UTF-8) from /etc/locale.gen

###### 12.3.2 Generate locale
```
locale-gen
```

###### 12.3.3 Create locale.conf
```
echo 'LANG=en_US.UTF-8' >> /etc/locale.conf
```

##### 12.4 Keyboard
Set your desired keyboard.
```
echo 'KEYMAP=us' >> /etc/vconsole.conf
```

##### 12.5 Hostname
Set your hostname
```
echo 'YOUR-HOSTNAME' >> /etc/hostname
```

##### 12.6 Create key file
```
dd bs=512 count=4 if=/dev/random of=/crypt/cryptkey.bin iflag=fullblock
chmod 600 /crypt/cryptkey.bin
cryptsetup -s 512 -h sha512 luksAddKey /dev/disk_p2 /crypt/key.bin
```

##### 12.7 Edit mkinitcpio.conf
For hooks, order is improtant!
```
/etc/mkinitcpio.conf
BINARIES=(btrfs)
FILES=(/crypt/cryptkey.bin)
HOOKS=(systemd btrfs keyboard autodetect microcode modconf kms sd-vconsole block sd-encrypt filesystems fsck)
```

##### 12.8 EFI

###### 12.8.1 Kernel Parameters
```
/etc/drfault/grub
-------------------
GRUB_CMDLINE_LINUX_DEFAULT="rw loglevel=3 rd.luks.name=UUID_of_dev_disk_p2=cryptroot rd.luks.key=UUID_of_dev_disk_p2=/crypt/cryptkey.bin rd.luks.options=UUID_of_dev_disk_p2=timeout=10s,discard,password-echo=yes,tries=1,keyfile-timeout=10s rootflags=subvol=@/0/snapshot root=/dev/mapper/cryptroot resume=UUID=UUID_of_dev_mapper_cryptroot resume_offset=XXXX"
```
Find UUID with

`blkid -s UUID -o value /dev/disk_p2`

`blkid -s UUID -o value /dev/mapper/cryptroot`

Find resume offset with

`btrfs inspect-internal map-swapfile -r /swap/swapfile`


##### 12.9 Generate mkinitcpio image
```
mkinitcpio -P
```

##### 12.10.1 Configure grub [Require version 2.12rc1 +]

Enable cryptodisk

```
/etc/default/grub
GRUB_ENABLE_CRYPTODISK=y
# If dual boot with windows then this one too
GRUB_DISABLE_OS_PROBER=false
```
Install grub
`grub-install --target=x86_64-efi --efi-directory=/efi --bootloader-id=GRUB --disable-shim-lock --sbat /usr/share/grub/sbat.csv --modules="all_video boot btrfs cat chain configfile echo efifwsetup efinet ext2 fat font gettext gfxmenu gfxterm_background gfxterm gzio halt help hfsplus iso9660 jpeg keystatus loadenv loopback linux ls lsefi lsefimmap lsefisystab lssal memdisk minicmd normal ntfs part_apple part_msdos part_gpt password_pbkdf2 png probe reboot regexp search search_fs_uuid search_fs_file search_label sleep smbios squash4 test true video xfs zfs zfscrypt zfsinfo play cpuid tpm cryptodisk luks luks2 lvm mdraid09 mdraid1x raid5rec raid6rec gcry_sha512 gcry_sha256"`

Generate config
`grub-mkconfig -o /boot/grub/grub.cfg`

##### 12.10.2 Secure Boot
```
# Create keys
sbctl create-keys

# Here "-m" option is IMPORTANT! You can softbrick your device if you dont use it. It will basically add microsoft keys if available.
sbctl enroll-keys -m

# Sign files
sbctl sign -s /boot/vmlinuz-linux
sbctl sign -s /efi/EFI/GRUB/grubx64.efi
```
##### 12.11 Root password
```
passwd
```

##### 12.12 Snapper
```
umount /.snapshots/
rmdir /.snapshots/
snapper --no-dbus -c root create-config /
rmdir /.snapshots/
mkdir /.snapshots/
mount /.snapshots/
systemctl enable /usr/lib/systemd/system/snapper*
```

##### 12.13 Add User
```
useradd -U -G wheel,video,audio,storage -m --btrfs-subvolume-home myuser
snapper --no-dbus -c myuser create-config /home/myuser
passwd myuser
```

##### 12.14 Move Pacman Database
```
/etc/pacman.conf
----------------
# Change DBPath=/var/lib/pacman to
DBPath=/usr/lib/pacman
```
Then Move original /var/lib/pacman dir to /usr/lib
```
mv /var/lib/pacman /usr/lib
```
#### Reboot
```
# Exit chroot
exit

# Remove swap
swapoff -a

# Unmount /mnt and all partition
umount -R /mnt

# Close crypt partition
cryptsetup close cryptroot

reboot
```

#### Rollbacks
> todo
